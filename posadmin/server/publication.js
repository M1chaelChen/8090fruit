Meteor.publish("receipts", function(){
	return Receipts.find();
})

Meteor.publish("table", function () {
    return Table.find();
});

Meteor.publish("book", function () {
	return Book.find();
})