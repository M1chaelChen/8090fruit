Meteor.myFunctions ={
    receiptInc: function(name){
        var ret = ReceiptSeq.findOne({_id:name});
        var temp = parseInt(ret.increment) +1;
        ReceiptSeq.update({_id:name},{$set:{increment: temp}});
        return temp;
    }
};




Meteor.methods({

    check_key: function(key) {
        var keycheck1 = key.indexOf("0811");
        var keycheck2 = key.indexOf("0930");
        var keycheck3 = key.indexOf("7166");
        var keycheck4 = key.indexOf("8598");
        var keycheck5 = key.indexOf("1108");
        var keycheck6 = key.indexOf("5610");
        var keycheck7 = key.indexOf("6789");
        var keycheck8 = key.indexOf("7890");
        if (keycheck1 == -1 && keycheck2 == -1 && keycheck3 == -1 && keycheck4 == -1 && keycheck5 == -1 && keycheck6 == -1 && keycheck7 == -1 && keycheck8 == -1) {
            return false;
        } else {
            return true;
        }
    },

    cart_to_transaction: function (cartid) {
        var cart=Cart.findOne(cartid);
        delete cart._id;
        Transactions.insert(cart);
       // Cart.remove(cart._id);
    },
    update_check_status: function(cartid, itemseq, checked){
        Cart.update({_id:cartid, "orders.seqnumber": itemseq},{ $set: {"orders.$.checked": checked}});
        return "ok!";
    },
    update_cart_check: function(cartid, checked){
        var cart = Cart.findOne({_id: cartid});
        for (var i=0; i<cart.orders.length; i++){
            var orderseq = cart.orders[i].seqnumber;
            Meteor.call('update_check_status', cart._id, orderseq, checked);
        }
    },
    split_divider:function(cartid, itemseq, divider){
        Cart.update({_id:cartid, "orders.seqnumber": itemseq},{ $set: {"orders.$.split_divider": divider}});
        return "ok!";
    },
    cart_divider:function(cartid, divider){
        var cart = Cart.findOne({_id:cartid});
        for (var i=0; i<cart.orders.length; i++){
            var orderseq = cart.orders[i].seqnumber;
            Meteor.call('split_divider',cart._id, orderseq, divider);
        }
    },
    cart_to_receipts:function(cartid){
        var cart = Cart.findOne({_id:cartid});
        var rec_seqnumber = Meteor.myFunctions.receiptInc("userid");

        Receipts.insert({
            seqnumber:rec_seqnumber,
            cartid:cart._id,
            cartseq:cart.seqnumber,
            tableid:cart.tableid,
            time:new Date(),
            ordernames:""
        });

        var subtotal_portion = 0;
        var gst_portion = 0;
        var pst_portion = 0;
        var subtotal = 0;
        var gst_total = 0;
        var pst_total = 0;

        for(var i=0; i<cart.orders.length; i++){
            if (cart.orders[i].checked) {
                var split = cart.orders[i].split_divider; 
                subtotal_portion = (parseFloat(cart.orders[i].price)/parseInt(cart.orders[i].split_divider)).toFixed(2);
                gst_portion = (parseFloat(cart.orders[i].gst)/parseInt(cart.orders[i].split_divider)).toFixed(2);
                pst_portion = (parseFloat(cart.orders[i].pst)/parseInt(cart.orders[i].split_divider)).toFixed(2);

                subtotal = (parseFloat(subtotal) + parseFloat(subtotal_portion)).toFixed(2);
                gst_total = (parseFloat(gst_total) + parseFloat(gst_portion)).toFixed(2);
                pst_total = (parseFloat(pst_total) + parseFloat(pst_portion)).toFixed(2);

                var order_name = cart.orders[i].name;
                var order_seqnumber = cart.orders[i].seqnumber;
                var order_specreq = cart.orders[i].specreq;

                Meteor.call('update_receipt', rec_seqnumber, order_seqnumber, order_name, order_specreq, subtotal_portion, gst_portion, pst_portion, split);

                Cart.update({_id:cart._id, "orders.seqnumber":order_seqnumber},{$set:{"orders.$.printed":true}});
            }
        };

        var amount = (parseFloat(subtotal) + parseFloat(gst_total) + parseFloat(pst_total)).toFixed(2);

        Receipts.update({"seqnumber":rec_seqnumber},{$set:{"subtotal": subtotal}});
        Receipts.update({"seqnumber":rec_seqnumber},{$set:{"gst": gst_total}});
        Receipts.update({"seqnumber":rec_seqnumber},{$set:{"pst": pst_total}});
        Receipts.update({"seqnumber":rec_seqnumber},{$set:{"amount": amount}});

        Cart.update({_id:cartid}, { $push: { receipts: {
            receipts_number:rec_seqnumber,
            paid:false
        }}});

        receipt = Receipts.findOne({"seqnumber":rec_seqnumber});
        return receipt;
    },
    update_receipt: function(rec_seqnumber, order_seqnumber, order_name, order_specreq, subtotal_portion, gst_portion, pst_portion, split){
        Receipts.update({"seqnumber":rec_seqnumber}, {$push: { orders: {
            name:order_name,
            seqnumber:order_seqnumber,
            specreq:order_specreq,
            split_divider:split,
            subtotal_portion:subtotal_portion,
            gst_portion:gst_portion,
            pst_portion:pst_portion
        }}});
        var receipt = Receipts.findOne({"seqnumber":rec_seqnumber});
        var ordername = receipt.ordernames;
        ordername = ordername + order_name + ", ";
        Receipts.update({"seqnumber":rec_seqnumber},{$set: {"ordernames":ordername}});
    },
    itemUpdateCartPrice: function (cartid, cartsub, cartgst, cartpst, carttotal) {
        Cart.update({_id: cartid}, {
            $set: {
                alert: true,
                subtotal: cartsub,
                gsttotal: cartgst,
                psttotal: cartpst,
                total: carttotal
            }
        });
        return true;
    },
    itemUpdateItemPrice: function (cartid, itemseq, itemprice, itemgst, itempst) {
        Cart.update({_id: cartid, "orders.seqnumber": itemseq}, {
            $set: {
                "orders.$.price": itemprice,
                "orders.$.gst": itemgst,
                "orders.$.pst": itempst
            }
        });
        return true;
    },
    cartUpdatePrice: function (cartid, discount) {
        Cart.update({_id: cartid}, {
            $set: {
                alert: true,
                subtotal: 0,
                gsttotal: 0,
                psttotal: 0,
                total: 0
            }
        });

        var subtotal = 0;
        var gsttotal = 0;
        var psttotal = 0;
        var total = 0;

        var cart = Cart.findOne({_id: cartid});
        for (var i = 0; i < cart.orders.length; i++) {
            var itemseq = cart.orders[i].seqnumber;
            var oldprice = cart.orders[i].price;
            var gstrate = cart.orders[i].gstrate;
            var pstrate = cart.orders[i].pstrate;
            var newprice = (parseFloat(oldprice) * parseFloat(discount)).toFixed(2);
            var gst = (parseFloat(newprice) * parseFloat(gstrate)).toFixed(2);
            var pst = (parseFloat(newprice) * parseFloat(pstrate)).toFixed(2);
            Cart.update({_id: cartid, "orders.seqnumber": itemseq}, {
                $set: {
                    "orders.$.price": newprice,
                    "orders.$.gst": gst,
                    "orders.$.pst": pst
                }
            });

            subtotal = (parseFloat(subtotal) + parseFloat(newprice)).toFixed(2);
            gsttotal = (parseFloat(gsttotal) + parseFloat(gst)).toFixed(2);
            psttotal = (parseFloat(psttotal) + parseFloat(pst)).toFixed(2);
            total = (parseFloat(subtotal) + parseFloat(gsttotal) + parseFloat(psttotal)).toFixed(2);
            Cart.update({_id: cartid}, {
                $set: {
                    subtotal: subtotal,
                    gsttotal: gsttotal,
                    psttotal: psttotal,
                    total: total,
                    discount:discount
                }
            });
        }
        return "ok";
    },
    printed:function(cartid,orderseq){
        var cart = Cart.findOne({_id:cartid});
        var tableid = cart.tableid;
        Table.update({name:tableid},{$set:{'bing':false}});
        Cart.update({_id:cart._id, "orders.seqnumber":orderseq}, { $set: { "orders.$.newitem":false}});
    },
    table_finish:function(tablename){
        Table.update({"name":tablename},{$set:{"inuse":false}});
        return "ok";
    },
    change_table:function(oldtableid,newtableid){
        var oldtable = Table.findOne({"name":oldtableid});
        Table.update({"name":newtableid},{$set:{"inuse":oldtable.inuse, "bing":oldtable.bing}});
        Table.update({"name":oldtableid},{$set:{"inuse":false, "bing":false}});
        return '哦了！';
    },
    check_table_free:function(tableid){
        var table = Table.findOne({"name":tableid});
        if (table.inuse) {
            return false;
        } else {
            return true;
        }
    },
    cashpay:function(receiptid, cartid){
        var receipt = Receipts.findOne({_id:receiptid});
        Receipts.update({_id:receiptid},{ $set: {"paid":true}});
        Receipts.update({_id:receiptid},{ $set: {"cashpay":receipt.amount}});
        Receipts.update({_id:receiptid},{ $set: {"cardpay":""}});
        Cart.update({_id:cartid, "receipts.receipts_number":receipt.seqnumber},{$set:{"receipts.$.paid":true}});
        var book = Book.findOne({"name":"today"});
        var booksales = book.sales;
        booksales = (parseFloat(booksales) + parseFloat(receipt.amount)).toFixed(2);
        Book.update({"name":"today"},{ $set: {"sales":booksales}});
        var bookcash = book.cash;
        bookcash = (parseFloat(bookcash) + parseFloat(receipt.amount)).toFixed(2);
        Book.update({"name":"today"},{ $set: {"cash":bookcash}});
    },
    cardpay:function(receiptid, tips, cartid){
        var receipt = Receipts.findOne({_id:receiptid});
        // Receipts.update({_id:receiptid},{ $set: {"paid":true, "card_receipt":cardreceipt, "tips":tips}});
        Receipts.update({_id:receiptid},{ $set: {"paid":true, "tips":(parseFloat(tips)).toFixed(2)}});
        Receipts.update({_id:receiptid},{ $set: {"cashpay":""}});
        Receipts.update({_id:receiptid},{ $set: {"cardpay":receipt.amount}});
        Cart.update({_id:cartid, "receipts.receipts_number":receipt.seqnumber},{$set:{"receipts.$.paid":true}});
        var book = Book.findOne({"name":"today"});

        var booksales = book.sales;
        booksales = (parseFloat(booksales) + parseFloat(receipt.amount)).toFixed(2);
        Book.update({"name":"today"},{ $set: {"sales":booksales}});
        var bookcard = book.card;
        bookcard = (parseFloat(bookcard) + parseFloat(receipt.amount)).toFixed(2);
        Book.update({"name":"today"},{ $set: {"card":bookcard}});
        var booktips = book.tips;
        booktips = (parseFloat(booktips) + parseFloat(tips)).toFixed(2);
        Book.update({"name":"today"},{ $set: {"tips":booktips}});
        var bookcash = book.cash;
        bookcash = (parseFloat(bookcash) - parseFloat(tips)).toFixed(2);
        Book.update({"name":"today"},{ $set: {"cash":bookcash}});
    },
    delete_item:function(cartid,orderseq){
        Cart.update({_id:cartid, "orders.seqnumber":orderseq},{$set:{"orders.$.voided":true}});
        Cart.update({_id:cartid, "orders.seqnumber":orderseq},{$set:{"orders.$.printbypass":true}});
        Cart.update({_id:cartid, "orders.seqnumber":orderseq},{$set:{"orders.$.specreq":'---已取消---'}});
        Cart.update({_id:cartid, "orders.seqnumber":orderseq},{$set:{"orders.$.checked":false}});
        Cart.update({_id:cartid, "orders.seqnumber":orderseq},{$set:{"orders.$.newitem":false}});
        Cart.update({_id:cartid},{$set:{"alert":true}});
        return 'ok';
    }
});