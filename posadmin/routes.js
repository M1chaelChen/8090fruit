Router.route('/posadmin/tablelist/:tablelistid', {
    action: function () {
        Meteor.subscribe("cart");
        Session.set('tableid',this.params.tablelistid);
        this.render('table_order',{
            data: function () {
                return {
                    tablelistid:this.params.tablelistid
                };
            }
        });
        this.layout('mainlayout', {
            data: {tablelistid:this.params.tablelistid}
        });
    }
});


Router.route('/posadmin/tablelist', {
    action: function () {
        Meteor.subscribe("cart");
        this.render('pos_tablelist');
        this.layout('poslayout');

    }
});
