Template.table_order.helpers({
    cart:function(){
        //var cart = Cart.findOne({tableid:Session.get('tableid'), "activated":true});

        return Cart.findOne({tableid:Session.get('tableid'), "activated":true});
    },
    tableid:function(){
        return this.tablelistid;
    },
    orders:function(){
        var cart= Cart.findOne({'tableid':Session.get('tableid'), "activated": true});
        if(cart==undefined)
            return [];
        return cart.orders;
    }
});

Template.registerHelper('equal', function(a,b){
    return a === b;
});

Template.table_order.events({
    'click #addorder':function(event){
        event.preventDefault();
        var tableid = Session.get('tableid');
        // alert(tableid);
        Router.go('/menu/table/'+tableid);
    },
    'change .check-checkbox':function(e){
        var checked = e.target.checked;
        var seqnumber = $(e.target).data('seqnumber');
        var cart = Session.get('cart');
        Meteor.call('update_check_status', cart._id, seqnumber, checked);
    },
    'change .split_select':function(e){
        var seqnumber = $(e.target).data('seqnumber');
        var selection = $('#item-'+seqnumber+'-divider').val();
        var cart = Session.get('cart');
        Meteor.call('split_divider', cart._id, seqnumber, selection);
    },
    'change .cart-checkbox':function(e){
        var checked = e.target.checked;
        var cart = Session.get('cart');
        Meteor.call('update_cart_check', cart._id, checked);
    },
    'change .cart_split':function(e){
        var selection = $('#cart-divider').val();
        var cart = Session.get('cart');
        Meteor.call('cart_divider', cart._id, selection);
    },
    /*
    'change .fendan-checkbox':function(e){
        var checked= e.target.checked;
        var seqnumber= $(e.target).data('seqnumber');
        var fendan_number=$('#item-'+seqnumber+'-divider').val();

        var fendan_array=Session.get('fendan_array');
        if(fendan_array==undefined){
            fendan_array=[];
        }

        var matched_item_entry=-1;
        for(var i=0;i<fendan_array.length;i++){
            if(fendan_array[i].seqnumber==seqnumber){
                matched_item_entry=i;
                break;
            }
        }

        if(matched_item_entry==-1&&checked){
            //if this item checkbox is checked and there is no entry in the array for this item
            //add it to the array
            fendan_array.push({
                seqnumber:seqnumber,
                fendan_number:fendan_number
            });
        }else if(matched_item_entry>-1 && !checked){
            //if this item checkbox is unchecked and there is entry in the array for this item
            //remove it from the array
            fendan_array.splice(matched_item_entry,1);
        }
        Session.set('fendan_array',fendan_array);
        console.log(fendan_array);

    },
    */
    'click #kitchenprintbtn':function(e){
        var cart = Session.get('cart');

        var doc={
            orders:[]
        };

        for (var i=0; i<cart.orders.length; i++){
            if (cart.orders[i].newitem){
                var print_name = cart.orders[i].name + " " + cart.orders[i].specreq;
                var item ={
                    name:print_name,
                    price:cart.orders[i].price,
                    gst:cart.orders[i].gst,
                    pst:cart.orders[i].pst
                };
                doc.orders.push(item);
            }
        }

        Meteor.call('print_table_orders',{
            tableid:cart.tableid,
            orders:doc.orders,
            cartseqnumber:cart.seqnumber
        });



        var kitchenObj={
            cartseqnumber:cart.seqnumber,
            tableid:cart.tableid,
            orders:[]
        };
        kitchenObj.cartseqnumber=kitchenObj.cartseqnumber.toFixed(0);

        for (var i=0; i<cart.orders.length; i++){
            var item = cart.orders[i];
            if (item.newitem) {
                if(item.printbypass!=true){
                    kitchenObj.orders.push({
                        menuitem_name:item.name,
                        notes:item.specreq,
                        itemseqnumber:item.seqnumber.toFixed(0)
                    });
                   
                }
                Meteor.call('printed', cart._id, item.seqnumber, function(err,res){
                    console.log('ok');
                });
            }
        }
        Meteor.call('print_kitchenline',kitchenObj);

    },
    'click #kitchenprintbtn2':function(e){
        var cart = Session.get('cart');
        var dataUrl = document.getElementById('kitchencanvas').toDataURL();
        var windowContent = '<!DOCTYPE html>';
        windowContent += '<html>';
        windowContent += '<head><title>Kitchen Canvas</title></head>';
        windowContent += '<body>';
        windowContent += '<img src="' + dataUrl + '">';
        windowContent += '</body>';
        windowContent += '</html>';
        var printWin = window.open('','','width=340, height=260');
        printWin.document.open();
        printWin.document.write(windowContent);
        printWin.document.close();
        printWin.focus();
        printWin.print();
        printWin.close();
    },
    'click #kitchenprint':function(e){
        Meteor.call('print_kitchenline',{
            menuitem_name:'test menu item',
            notes:'no celantral',
            tableid:'0001'
        });
    },
    'click #recieptprint':function(e){

        Meteor.call('print_reciept',{
            seqnumber:'0001',
            subtotal:'123',
            gsttotal:'312',
            psttotal:'122',
            total:'1',
            tableid:'0001',
            orders:[
                {name:'order1', price:'123',gst:'1',pst:'2'},
                {name:'order2', price:'123',gst:'1',pst:'2'},
                {name:'order3', price:'123',gst:'1',pst:'2'},
                {name:'order4', price:'123',gst:'1',pst:'2'},
                {name:'order5', price:'123',gst:'1',pst:'2'},
                {name:'order6', price:'123',gst:'1',pst:'2'},
                {name:'order7', price:'123',gst:'1',pst:'2'}
            ]
        });
    },
    'click #orderprint':function(e){
        Meteor.call('print_table_orders',{
            tableid:'0001',
            orders:[
                {name:'order1', price:'123',gst:'1',pst:'2'},
                {name:'order2', price:'123',gst:'1',pst:'2'},
                {name:'order3', price:'123',gst:'1',pst:'2'},
                {name:'order4', price:'123',gst:'1',pst:'2'},
                {name:'order5', price:'123',gst:'1',pst:'2'},
                {name:'order6', price:'123',gst:'1',pst:'2'},
                {name:'order7', price:'123',gst:'1',pst:'2'}
            ]
        });
    },

    // table buttons
    /*
    'click #kitchenprintbtn':function(event){
        event.preventDefault();
        alert('ok!');
    },
    */
    'click #changetablebtn':function(event){
        event.preventDefault();
        $('#changetable_modal_id').modal('show');
    },
    'click #checkbtn':function(event){
        event.preventDefault();
        var cart = Session.get('cart');
        if (cart.checked){
            Cart.update({_id:cart._id},{$set:{"printed":true}});
        }
        Meteor.call('cart_to_receipts', cart._id, function(err, res){
            //var receipt = Receipts.findOne({"seqnumber":res});
            // alert(res.seqnumber);
            Session.set('current_receipt',res);
            //var res123 = Session.get('current_receipt');
            //alert(res123.seqnumber);
        });
        var receipt = Session.get("current_receipt");
        //alert(receipt.seqnumber);
        $('#checktable_modal_id').modal('show');
    },
    'click #paymentbtn':function(event){
        event.preventDefault();
        var cart = Session.get('cart');
        Session.set('selected_receipt',null);
        $('#payment_modal_id').modal('show'); 
    },
    'click #addnotesbtn':function(event){
        event.preventDefault();
        $('#addnotes_modal_id').modal('show');
    },
    'click #finishbtn':function(e){
        event.preventDefault();
        $('#finish_modal_id').modal('show');

        /*

        

        var cart = Session.get('cart');
        
        if (cart.paid){
            Cart.update({_id:cart._id},{ $set: {"activated":false}});
        } else {
            alert('还未完成买单！');
        };
        
        var tableid = cart.tableid;

        Meteor.call("table_finish", tableid, function(err,res){
            alert(res);
        });
        Cart.update({_id:cart._id},{ $set: {"activated":false}});
        //Router.go('/posadmin/tablelist');
        */
    },
    // buttons in the modals
    "click .editpricebtn":function(event){
        event.preventDefault();
        Session.set('editprice_modal_data',this);
        var cart = Cart.findOne({tableid:Session.get('tableid'), "activated":true});
        Session.set('cart',cart);
        $('#editprice_modal_id').modal('show');
    },
    'click .delitembtn':function(event){
        event.preventDefault();
        Session.set('delitem_modal_data',this);
        var cart = Cart.findOne({tableid:Session.get('tableid'), "activated":true});
        Session.set('cart',cart);
        $('#delitem_modal_id').modal('show');
    },
    "click .carteditpricebtn":function(event){
        event.preventDefault();
        $('#carteditprice_modal_id').modal('show');
    }
});

// change table module

Template.changetable_modal.helpers({
    modalcart:function(){
        return Session.get('cart');
    }
});

Template.finish_modal.events({
    'submit #key_form':function(event){
        event.preventDefault();
        var cart = Session.get('cart');
        var tableid = cart.tableid;
        var key_card = $('#key_card').val();
        Meteor.call("check_key", key_card, function(err, res){
            if (res == true) {
                Meteor.call("table_finish", tableid, function(err,res){});
                Cart.update({_id:cart._id},{ $set: {"activated":false}});
                $('#finish_modal_id').modal('hide');
            } else {
                $('#finish_modal_id').modal('hide');
                alert("密码错误");
            }
        })
    }
});

Template.changetable_modal.events({
    'submit #changetable':function(event){
        event.preventDefault();
        var cart = Session.get('cart');
        var oldtable = cart.tableid;
        var newtable = $('#newtable').val();
        var e_count = 0;
        var table_array =["s1","s2","s3","s4","s5","s6","s7","m1","m2","m3","m4","b1","b2","b3","t1","t2","t3"];
        for (var i=0; i<table_array.length; i++){
            if(table_array[i] == newtable){
                e_count += 1;
            }
        }

        if (e_count == 0){
            alert('没这个桌子啊');
        } else {
            if (oldtable == newtable) {
                alert('不能换自己');
            } else {
                Meteor.call('check_table_free',newtable,function(err,res){
                    if (res) {
                        Cart.update({"_id":cart._id},{ $set: { "tableid":newtable }});
                        Meteor.call('change_table', oldtable, newtable, function(err,res){
                            alert(res);
                        });   
                    } else {
                        alert('桌子被用着呢！');
                    }
                });
            }
        }
        $('#changetable_modal_id').modal('hide');
        
        // Router.go('/posadmin/tablelist');
    }
});

// check out table module

Template.checktable_modal.helpers({
    modalcart:function(){
        return Session.get('cart');
    },
    checklist:function(){
        return Session.get('fendan_array');
    },
    checkitem:function(){
        var current_receipt = Session.get("current_receipt");// Receipts.findOne({"seqnumber": Session.get('receipt_seqnumber')});
        return current_receipt;
    }
});

Template.checktable_modal.events({
    'click #checkconfirmbtn':function(event){
        event.preventDefault();
        var cart = Session.get('cart');
        Cart.update({_id:cart._id},{$set:{'receipt_printed':true}});
        // Meteor.call('tableprinted', cart._id);
        print_reciept(Session.get("current_receipt"));
        // alert('print here..');
        $('#checktable_modal_id').modal('hide');
    }
});

Template.payment_modal.helpers({
    current_cart:function(){
        var cart = Session.get('cart');
        var cartid = cart._id;
        return Cart.findOne({_id:cartid});
    },
    current_receipt:function(){
        var receipt = Session.get('selected_receipt');
        return receipt;
    }
});

Template.payment_modal.events({
    'click #paymentconfirmbtn':function(event){
        event.preventDefault();
        print_reciept(Session.get("selected_receipt"));
        //$('#payment_modal_id').modal('hide');
    },
    'click .select_receipt_btn':function(e){
        event.preventDefault();
        var selection = e.target.value;
        var selected_receipt = Receipts.findOne({"seqnumber":parseInt(selection)});
        Session.set("selected_receipt",selected_receipt);
    },
    /*
    'change #select_receipt':function(e){
        event.preventDefault();
        var selection = $('#select_receipt').val();
        var selected_receipt = Receipts.findOne({"seqnumber":parseInt(selection)});
        Session.set("selected_receipt",selected_receipt);
    }, */
    'click #cash_confirm_btn':function(e){
        event.preventDefault();
        var receipt = Session.get('selected_receipt');
        if (receipt==null) {
            alert('没有选择小票！');
        } else {
            var cart = Session.get('cart');
            var amount_paid = $('#paid_amount').val();
            var change = (parseFloat(amount_paid) - parseFloat(receipt.amount)).toFixed(2);
            if (amount_paid=="") {
                alert('金额不能为空!');
            } else if (change<0) {
                alert('付款额不够!');
            } else {
                Session.set('changes',change);
                // alert('请找零: $ ' + change);
                Meteor.call('cashpay', receipt._id, cart._id);
                Cart.update({_id:cart._id},{$set:{'paid':true}});
                receipt.paid = true;
                Session.set('selected_receipt', receipt);
                $('#payment_modal_id').modal('hide');
                $('#cash_payalert_modal_id').modal('show');
            }
        }
    },
    'click #card_confirm_btn':function(e){
        event.preventDefault();
        var receipt = Session.get('selected_receipt');
        if (receipt==null) {
            alert('没有选择小票！');
        } else {
            var cart = Session.get('cart');
            var tips = $('#paid_amount').val();
            if (tips=="") {
                alert('金额不能为空！');
            } else {
                tips = (parseFloat(tips)).toFixed(2);
                Session.set('tips',tips);
                Meteor.call('cardpay', receipt._id, tips, cart._id);
                Cart.update({_id:cart._id},{$set:{'paid':true}});
                receipt.paid = true;
                Session.set('selected_receipt', receipt);
                $('#payment_modal_id').modal('hide');
                $('#card_payalert_modal_id').modal('show');
            }
        } 
    },
    'click .numpad':function(event){
        event.preventDefault();
        var typednum = event.target.value;
        var inputbox = $('#paid_amount').val();
        inputbox += typednum;
        $('#paid_amount').val(inputbox);
    },
    'click .numpad-del':function(event){
        event.preventDefault();
        $('#paid_amount').val("");

    }
});

Template.cash_payalert_modal.helpers({
    changes:function(){
        return Session.get('changes');
    }
});


Template.cash_payalert_modal.events({
    'click .cash_payalert_confirm_btn':function(event){
        event.preventDefault();
        Session.set('changes','');
    }
});


Template.card_payalert_modal.helpers({
    tips:function(){
        return Session.get('tips');
    }
});

Template.card_payalert_modal.events({
    'click .card_payalert_confirm_btn':function(event){
        event.preventDefault();
        Session.set('tips','');
    }
});

// add notes/comments moodule

Template.addnotes_modal.helpers({
    modalcart:function(){
        return Session.get('cart');
    }
});

Template.addnotes_modal.events({
    'submit #addnotes':function(event){
        event.preventDefault();
        var cart = Session.get('cart');
        comment = $('#ordernotes').val();
        Cart.update({"_id":cart._id} ,{ $push: { "comments" : comment}});
        alert("ok!");
    }
});

Template.delitem_modal.events({
    "click #delitemyes":function(event){
        event.preventDefault();
        // update item price to zero
        var cart = Session.get("cart");
        var price = $('#newprice').val();
        var item = Session.get('delitem_modal_data');

        var olditemprice = item.price;
        var olditemgst = item.gst;
        var olditempst = item.pst;
        
        var oldcartsubtotal = cart.subtotal;
        var oldcartgsttotal = cart.gsttotal;
        var oldcartpsttotal = cart.psttotal;
        var oldcarttotal = cart.total;

        var newitemprice = (0).toFixed(2);
        var newitemgst = (0).toFixed(2);
        var newitempst = (0).toFixed(2);

        var newcartsubtotal = (parseFloat(oldcartsubtotal) - parseFloat(olditemprice)).toFixed(2);
        var newcartgsttotal = (parseFloat(oldcartgsttotal) - parseFloat(olditemgst)).toFixed(2);
        var newcartpsttotal = (parseFloat(oldcartpsttotal) - parseFloat(olditempst)).toFixed(2);
        var newcarttotal = (parseFloat(newcartsubtotal) + parseFloat(newcartgsttotal) + parseFloat(newcartpsttotal)).toFixed(2);

        
        Meteor.call('itemUpdateItemPrice',
                    cart._id,
                    item.seqnumber,
                    newitemprice,
                    newitemgst,
                    newitempst,
                    function(err,res){
        });

        Meteor.call('itemUpdateCartPrice',
                    cart._id,
                    newcartsubtotal,
                    newcartgsttotal,
                    newcartpsttotal,
                    newcarttotal,
                    function(err,res){
        });

        // now set item void boolean to true
        Meteor.call('delete_item',cart._id,item.seqnumber,function(err,res){
            console.log(res);
            cart = Cart.findOne({'tableid':Session.get('tableid'), "activated": true});
            Session.set('cart',cart);
            $('#delitem_modal_id').modal('hide');
        });

    },
    "click #delitemno":function(event){
        event.preventDefault();
        $('#delitem_modal_id').modal('hide');
    }
})

Template.editprice_modal.helpers({
    data:function(){
        return Session.get('editprice_modal_data');
    },
    cartmodal:function(){
        var cartid = Session.get('editprice_modal_data');
        return Cart.findOne({_id:cartid});
    }
});

Template.editprice_modal.events({
    "submit #editpriceform": function (event) {
        // Prevent default browser form submit
        event.preventDefault();

        // Get value from form element
        var cart = Session.get("cart");
        var price = $('#newprice').val();
        var item = Session.get('editprice_modal_data');

        var olditemprice = item.price;
        var olditemgst = item.gst;
        var olditempst = item.pst;
        
        var oldcartsubtotal = cart.subtotal;
        var oldcartgsttotal = cart.gsttotal;
        var oldcartpsttotal = cart.psttotal;
        var oldcarttotal = cart.total;

        var newitemprice = parseFloat(price).toFixed(2);
        var newitemgst = (parseFloat(item.gstrate)*parseFloat(newitemprice)).toFixed(2);
        var newitempst = (parseFloat(item.pstrate)*parseFloat(newitemprice)).toFixed(2);

        var newcartsubtotal = (parseFloat(oldcartsubtotal) - parseFloat(olditemprice) + parseFloat(newitemprice)).toFixed(2);
        var newcartgsttotal = (parseFloat(oldcartgsttotal) - parseFloat(olditemgst) + parseFloat(newitemgst)).toFixed(2);
        var newcartpsttotal = (parseFloat(oldcartpsttotal) - parseFloat(olditempst) + parseFloat(newitempst)).toFixed(2);
        var newcarttotal = (parseFloat(newcartsubtotal) + parseFloat(newcartgsttotal) + parseFloat(newcartpsttotal)).toFixed(2);

        
        Meteor.call('itemUpdateItemPrice',
                    cart._id,
                    item.seqnumber,
                    newitemprice,
                    newitemgst,
                    newitempst,
                    function(err,res){
        });

        Meteor.call('itemUpdateCartPrice',
                    cart._id,
                    newcartsubtotal,
                    newcartgsttotal,
                    newcartpsttotal,
                    newcarttotal,
                    function(err,res){
        });

        alert("完成！");
        $('#editprice_modal_id').modal('hide');


    }
});

Template.carteditprice_modal.helpers({
    data:function(){
        return Session.get('cart');
    },
    cartmodal:function(){
        var cartid = Session.get('cart');
        return cartid;
    }
});

Template.carteditprice_modal.events({
    "submit #carteditpriceform": function (event) {
        // Prevent default browser form submit
        event.preventDefault();

        // Get value from form element
        var cart = Session.get("cart");
        var discount = $('#cartnewprice').val();

        Meteor.call('cartUpdatePrice',
                    cart._id,
                    discount,
                    function(err,res){
                        alert(res);
        });

        $('#carteditprice_modal_id').modal('hide');
        


    }
});


function print_reciept(current_reciept){
    var doc={
        orders:[]
    };

    for (var i=0; i<current_reciept.orders.length; i++){
        var theorder = current_reciept.orders[i];
        var item ={
            name:theorder.seqnumber + ' - ' +theorder.name+ ':X 1 / ' + theorder.split_divider ,
            specreq:theorder.specreq,
            price:theorder.subtotal_portion,
            gst:theorder.gst,
            pst:theorder.pst,
        };
        doc.orders.push(item);
    }

    Meteor.call('print_reciept',{
        tableid:Session.get('cart').tableid,
        seqnumber:current_reciept.seqnumber,
        cartseqnumber:current_reciept.cartseq,
        subtotal:current_reciept.subtotal,
        gsttotal:current_reciept.gst,
        psttotal:current_reciept.pst,
        total:current_reciept.amount,
        orders:doc.orders
    });
}