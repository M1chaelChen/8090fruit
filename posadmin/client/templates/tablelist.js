Template.pos_tablelist.events({
    "click .tablelistbtn": function (event) {
        // Prevent default browser form submit
        event.preventDefault();
        var tablelistid = event.target.value;
        Session.set('cart', Cart.findOne({'tableid':tablelistid, "activated":true}));
        Session.set('tableid',tablelistid);
        Router.go('/posadmin/tablelist/'+tablelistid);

    }
});

Template.pos_tablelist.helpers({
	table_check: function(table){
		var tablename = String(table);
		return Table.findOne({'name':tablename});
	}
});