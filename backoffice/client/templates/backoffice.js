Template.backoffice.events({
    "submit #keyform":function(event){
        event.preventDefault();
        var key = $('#officekey').val();
        Meteor.call('keycheck',key,function(err,res){
            if (res) {
                Session.set('book',res);
                $('#backoffice_modal_id').modal('show');
            } else {
                alert('密码错误');
            }
        });
    }
});

Template.backoffice_modal.helpers({
    book: function(){
        return Session.get('book');
    }
});

Template.backoffice_modal.events({
    "click #systemreset":function(event){
        event.preventDefault();
        $('#backoffice_modal_id').modal('hide');
        $('#systemreset_modal_id').modal('show');
    },
    "click #csvdownload":function(event){
        event.preventDefault();
        CSVExporter.exportAll();
    }
});

Template.systemreset_modal.events({
    "click #systemresetyes":function(event){
        event.preventDefault();
        Meteor.call('systemreset',function(err,res){
            if (res) {
                $('#systemreset_modal_id').modal('hide');
                alert(res);
            } else {
                $('#systemreset_modal_id').modal('hide');
                alert("有错误");
            }
        });
    },
    "click #systemresetno":function(event){
        event.preventDefault();
        $('#systemreset_modal_id').modal('hide');
    }
});

/*
Template.backoffice.helpers({
    salestotal:function(){
        return Template.instance().reactiveVar_salesTotal.get();
    }
});

Template.backoffice.onCreated(function(){
    var self = this;
    Session.set('sales_interval','day');
    self.reactiveVar_salesTotal = new ReactiveVar("Waiting for response from server...");
    Deps.autorun(function () {
        Meteor.call('sales_total',Session.get('sales_interval'), function (err, asyncValue) {
            if (err)
                console.log(err);
            else
                self.reactiveVar_salesTotal.set(asyncValue==undefined?'0.00':asyncValue);
        });
    });

});

Template.backoffice.events({
    'change #sales_total_interval':function(e){
        Session.set('sales_interval',$(e.target).val());
    }
});

*/