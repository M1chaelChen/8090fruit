Template.registerHelper('equals', function (a, b) {
    return a === b;
});

Template.registerHelper('tillMoment', function(time){
    return moment(time).fromNow();
});

Template.orderslist.helpers({
    carts:function(){
        var carts= Cart.find({},{sort:{time:-1}});
        return carts;
    }
});


Template.orderslist.events({

    "click .orderdetails":function(event){
        event.preventDefault();
        // var tableselid = event.target.value;
        Session.set('masterorders_modal_data',this._id);
        $('#masterorders_modal_id').modal('show');
    }
});


Template.masterorders_modal.helpers({
    data:function(){
        return Session.get('masterorders_modal_data');
    },
    cartmodal:function(){
        var cartid = Session.get('masterorders_modal_data');
        return Cart.findOne({_id:cartid});
    }
});

