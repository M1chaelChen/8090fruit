

// -------------------------------------------------------------

// used by narasace
Receipts = new Mongo.Collection('receipts');

/*
 * receipts collection
 * {
 *  cartid
 *  cartseq
 *  time
 *  orders:[{
 *    car.orders.name
 *    cart.orders.seqnumber
 *    cart.orders.price / split_divider
 *    cart.orders.gst / split_divider
 *    cart.orders.pst / split_divider
 *  }]
 *  subtotal
 *  gst
 *  pst
 *  amount
 *  paid
 *  tips
 *  cash: boolean
 *  card: boolean
 *  card_receipt_digits
 *  paid: boolean
 * }
 */

// The book for bookkeeping
Book = new Mongo.Collection('book');

// -------------------------------------------------------------

/**
 * table collection
 * {
 *  name (str)
 *  order number (int)
 *  bing (boolean)
 * }
 */
Table = new Mongo.Collection('table');

/*
 * category collection
 * {
 *  name (str)
 *  pic_id (uri)
 * }
 *
 * */
Category = new Mongo.Collection("category");

/*
 * menuitem collection
 * {
 *  category (_id)
 *  name (str)
 *  price (float)
 *  pic_id (uri)
 *  item_id (str)
 *  ingred
 * }
 *
 * */
MenuItem = new Mongo.Collection("menuitem");

/*
 * cart collection
 * {
 *  tableid
 *  date
 *  order_id (int)
 *  orders:[{
 *      itemid
 *      name
 *      pic_id
 *      ingred
 *      status (pending/waiting/done/paid)   pending for customer just ordered. waiting when waitress comfirmed the orders
 *  }]
 * }
 *
 * */
Cart = new Mongo.Collection("cart");

/*
 *
 * orders collection
 * {
 *
 *
 * }
 * */

Orders = new Mongo.Collection("orders");

/*
 * transactions collection
 * {
 *  all the fields in cart
 *  paymethod (card/cash)
 * }
 * */

Transactions = new Mongo.Collection("transactions");

/*
 * sequence collection for carts and ordered items
 * {
 * _id: "userid",
 * seq: (int)
 *
 *
 * */

 OrderSeq = new Mongo.Collection("order_counter");

 ItemSeq = new Mongo.Collection("item_counter");

 ReceiptSeq = new Mongo.Collection("receipt_counter");




if(Meteor.isServer){
    /**
     * initialization
     */


    Transactions.remove({});
    Table.remove({});
    Category.remove({});
    MenuItem.remove({});
    Cart.remove({});
    Receipts.remove({});
    Book.remove({});
    OrderSeq.remove({});
    ItemSeq.remove({});
    ReceiptSeq.remove({});

    

    Book.insert({"name":"today", "sales":0, "cash":0, "card":0, "tips":0});

    OrderSeq.insert({_id:"userid", increment: parseInt(0)});
    ItemSeq.insert({_id:"userid", increment: parseInt(0)});
    ReceiptSeq.insert({_id:"userid", increment: parseInt(0)});

    Table.insert({name:"s1", order_number:"", inuse:false, bing:false});
    Table.insert({name:"s2", order_number:"", inuse:false, bing:false});
    Table.insert({name:"s3", order_number:"", inuse:false, bing:false});
    Table.insert({name:"s4", order_number:"", inuse:false, bing:false});
    Table.insert({name:"s5", order_number:"", inuse:false, bing:false});
    Table.insert({name:"s6", order_number:"", inuse:false, bing:false});
    Table.insert({name:"s7", order_number:"", inuse:false, bing:false});

    Table.insert({name:"m1", order_number:"", inuse:false, bing:false});
    Table.insert({name:"m2", order_number:"", inuse:false, bing:false});
    Table.insert({name:"m3", order_number:"", inuse:false, bing:false});

    Table.insert({name:"b1", order_number:"", inuse:false, bing:false});
    Table.insert({name:"b2", order_number:"", inuse:false, bing:false});
    Table.insert({name:"b3", order_number:"", inuse:false, bing:false});

    Table.insert({name:"t1", order_number:"", inuse:false, bing:false});
    Table.insert({name:"t2", order_number:"", inuse:false, bing:false});
    Table.insert({name:"t3", order_number:"", inuse:false, bing:false});

    var catid = Category.insert({name: "奶茶系列", pic_id: "dk.png"});
    MenuItem.insert({category: catid, seqnumber:'101', name: "经典奶茶", price: 4.25, pic_id: "dk.png", SR:1, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'102', name: "绿奶茶", price: 4.50, pic_id: "dk.png", SR:1, itemid:"", ingred: "",});
    MenuItem.insert({category: catid, seqnumber:'103', name: "椰香奶茶", price: 4.75, pic_id: "dk.png", SR:1, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'104', name: "布丁奶茶", price: 4.75, pic_id: "dk.png", SR:1, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'105', name: "胚芽奶茶", price: 4.75, pic_id: "dk.png", SR:1, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'106', name: "仙草奶茶", price: 4.75, pic_id: "dk.png", SR:1, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'107', name: "木瓜奶茶", price: 4.50, pic_id: "dk.png", SR:1, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'108', name: "红豆奶茶", price: 4.50, pic_id: "dk.png", SR:1, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'109', name: "抹茶奶茶", price: 4.95, pic_id: "dk.png", SR:1, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'110', name: "芋香奶茶", price: 4.50, pic_id: "dk.png", SR:1, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'111', name: "姜母奶茶", price: 4.75, pic_id: "dk.png", SR:1, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'112', name: "蜂蜜奶茶", price: 4.50, pic_id: "dk.png", SR:1, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'113', name: "玫瑰奶茶", price: 4.50, pic_id: "dk.png", SR:1, itemid:"", ingred: ""});
    

    catid =Category.insert({name: "红茶/绿茶系列", pic_id:"dk.png"});
    MenuItem.insert({category: catid, seqnumber:'201', name: "蜂蜜", price: 4.25, pic_id: "dk.png", SR:2, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'202', name: "百香果", price: 4.25, pic_id: "dk.png", SR:3, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'203', name: "荔枝", price: 4.25, pic_id: "dk.png", SR:3, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'204', name: "草莓", price: 4.25, pic_id: "dk.png", SR:3, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'205', name: "青苹果", price: 4.25, pic_id: "dk.png", SR:3, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'206', name: "金橘", price: 4.25, pic_id: "dk.png", SR:3, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'207', name: "凤梨", price: 4.25, pic_id: "dk.png", SR:3, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'208', name: "水蜜桃", price: 4.25, pic_id: "dk.png", SR:3, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'209', name: "香槟葡萄", price: 4.25, pic_id: "dk.png", SR:3, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'210', name: "桂圆红枣", price: 4.50, pic_id: "dk.png", SR:2, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'211', name: "冬瓜", price: 4.25, pic_id: "dk.png", SR:3, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'212', name: "姜母", price: 4.50, pic_id: "dk.png", SR:2, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'213', name: "玫瑰", price: 4.50, pic_id: "dk.png", SR:2, itemid:"", ingred: ""});
    
    catid =Category.insert({name: "柠檬茶系列", pic_id:"dk.png"});
    MenuItem.insert({category: catid, seqnumber:'301', name: "柠檬", price: 4.25, pic_id: "dk.png", SR:2, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'302', name: "金橘柠檬", price: 4.50, pic_id: "dk.png", SR:2, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'303', name: "蜂蜜柠檬", price: 4.50, pic_id: "dk.png", SR:2, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'304', name: "百香果柠檬", price: 4.50, pic_id: "dk.png", SR:3, itemid:"", ingred: ""});
   
    catid =Category.insert({name: "冰沙系列", pic_id:"dk.png"});
    MenuItem.insert({category: catid, seqnumber:'401', name: "芋头冰沙", price: 4.75, SR:0, pic_id: "dk.png", SR:4, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'402', name: "椰香冰沙", price: 4.75, pic_id: "dk.png", SR:4, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'403', name: "百香果冰沙", price: 4.75, pic_id: "dk.png", SR:4, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'404', name: "水蜜桃冰沙", price: 4.75, pic_id: "dk.png", SR:4, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'405', name: "荔枝冰沙", price: 4.75, pic_id: "dk.png", SR:4, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'406', name: "青苹果冰沙", price: 4.75, pic_id: "dk.png", SR:4, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'407', name: "草莓冰沙", price: 4.75, pic_id: "dk.png", SR:4, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'408', name: "蓝莓冰沙", price: 4.75, pic_id: "dk.png", SR:4, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'409', name: "柠檬冰沙", price: 4.75, pic_id: "dk.png", SR:4, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'410', name: "木瓜冰沙", price: 4.75, pic_id: "dk.png", SR:4, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'411', name: "芒果冰沙", price: 4.95, pic_id: "dk.png", SR:4, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'412', name: "凤梨冰沙", price: 4.75, pic_id: "dk.png", SR:4, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'413', name: "葡萄冰沙", price: 4.75, pic_id: "dk.png", SR:4, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'414', name: "红豆冰沙", price: 4.95, pic_id: "dk.png", SR:4, itemid:"", ingred: ""});

    catid =Category.insert({name: "多多冰沙", pic_id:"dk.png"});
    MenuItem.insert({category: catid, seqnumber:'501', name: "柠檬多多", price: 4.00, SR:0, pic_id: "dk.png", SR:4, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'502', name: "蓝莓多多", price: 4.00, pic_id: "dk.png", SR:4, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'503', name: "百香多多", price: 4.00, pic_id: "dk.png", SR:4, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'504', name: "葡萄多多", price: 4.00, pic_id: "dk.png", SR:4, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'505', name: "荔枝多多", price: 4.00, pic_id: "dk.png", SR:4, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'506', name: "金橘多多", price: 4.00, pic_id: "dk.png", SR:4, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'507', name: "青苹果多多", price: 4.00, pic_id: "dk.png", SR:4, itemid:"", ingred: ""});
    MenuItem.insert({category: catid, seqnumber:'508', name: "水蜜桃多多", price: 4.00, pic_id: "dk.png", SR:4, itemid:"", ingred: ""});
    

    MenuItem.update({},{$set:{gst: 0.05}},{multi:true});
    MenuItem.update({},{$set:{pst: 0.00}},{multi:true});
};
