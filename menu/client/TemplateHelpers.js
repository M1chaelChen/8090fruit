Meteor.myFunctions ={
    orderInc: function(name){
        var ret = OrderSeq.findOne({_id:name});
        var temp = parseInt(ret.increment) + 1;
        OrderSeq.update({_id:name},{$set:{increment: temp}});
        return temp;
    },
    itemInc: function(name){
        var ret = ItemSeq.findOne({_id:name});
        var temp = parseInt(ret.increment) +1;
        ItemSeq.update({_id:name},{$set:{increment: temp}});
        return temp;
    },
    receiptInc: function(name){
        var ret = ReceiptSeq.findOne({_id:name});
        var temp = parseInt(ret.increment) +1;
        ReceiptSeq.update({_id:name},{$set:{increment: temp}});
        return temp;
    }
};

Template.cart.helpers({
    orders:function(){
        var cart= Cart.findOne({'tableid':Session.get("tableid"), "activated":true});
        if(cart==undefined)
            return [];
        return cart.orders;
    },
    carttotal:function(){
        var cart= Cart.findOne({'tableid':Session.get("tableid"), "activated":true});
        if(cart==undefined)
            return [];
        return cart.total;
    },
    cartgsttotal:function(){
        var cart= Cart.findOne({'tableid':Session.get("tableid"), "activated":true});
        if(cart==undefined)
            return [];
        return cart.gsttotal;
    },
    cartpsttotal:function(){
        var cart= Cart.findOne({'tableid':Session.get("tableid"), "activated":true});
        if(cart==undefined)
            return [];
        return cart.psttotal;
    },
    cartsubtotal:function(){
        var cart= Cart.findOne({'tableid':Session.get("tableid"), "activated":true});
        if(cart==undefined)
            return [];
        return cart.subtotal;
    }
});

Template.categories.events({
    'click #search':function(){
        event.preventDefault();
        Router.go('/menu/table/'+Session.get('tableid')+'/menuitem/'+$('#menu_item_id').val());
    }
});

Template.categories.helpers({
    categories: function () {
        return Category.find({});
    }
});

Template.tablesel.events({
    "submit #form": function (event) {
        // Prevent default browser form submit
        event.preventDefault();

        // Get value from form element
        var text = $('#tableNumber').val();

        Router.go('/menu/table/'+text);
    },
    "click .tableselbtn": function (event) {
        event.preventDefault();
        var tableselid = event.target.value;
        Router.go('/menu/table/'+tableselid);
    }

});


function orderMenuItem(e){
    e.preventDefault();
    var tableid = Session.get("tableid");
    var self=this;
    var cartid = Meteor.call('findCart', tableid, function(err, res){
        if(!res){
            var sub = self.price.toFixed(2);
            var gstsub = (parseFloat(self.price) * parseFloat(self.gst)).toFixed(2);
            var pstsub = (parseFloat(self.price) * parseFloat(self.pst)).toFixed(2);

            var total = (parseFloat(sub) + parseFloat(gstsub) + parseFloat(pstsub)).toFixed(2);

            Cart.insert({
                seqnumber: Meteor.myFunctions.orderInc("userid"),
                tableid:tableid,
                time:new Date(),
                alert:false,
                activated:true,
                paid:false,
                orders:[{
                    seqnumber: Meteor.myFunctions.itemInc("userid"),
                    // itemid:self._id,
                    name:self.name,
                    printbypass:self.printbypass,
                    pic_id:self.pic_id,
                    specreq:" ",
                    price:self.price,
                    gst:gstsub,
                    pst:pstsub,
                    gstrate:self.gst,
                    pstrate:self.pst,
                    checked:true,
                    split_divider:1,
                    newitem:true,
                    printbypass:self.printbypass
                }],
                subtotal:sub,
                gsttotal:gstsub,
                psttotal:pstsub,
                total:total,
                // comments:[{}]
            });
        }else{
            var oldsub = res.subtotal;
            var oldgst = res.gsttotal;
            var oldpst = res.psttotal;
            var oldtotal = res.total;

            var tempsub = self.price.toFixed(2);
            var sub = (parseFloat(oldsub) + parseFloat(tempsub)).toFixed(2);
            var tempgst = (parseFloat(self.price) * parseFloat(self.gst)).toFixed(2);
            var gstsub = (parseFloat(oldgst) + parseFloat(tempgst)).toFixed(2);
            var temppst = (parseFloat(self.price) * parseFloat(self.pst)).toFixed(2);
            var pstsub = (parseFloat(oldpst) + parseFloat(temppst)).toFixed(2);

            var total = (parseFloat(tempsub) + parseFloat(tempgst) + parseFloat(temppst) + parseFloat(oldtotal)).toFixed(2);

            Cart.update(res._id,{ $push: { orders: {
                seqnumber:Meteor.myFunctions.itemInc("userid"),
                // itemid:self._id,
                name:self.name,
                pic_id:self.pic_id,
                specreq:" ",
                price:self.price,
                gst:tempgst,
                pst:temppst,
                gstrate:self.gst,
                pstrate:self.pst,
                checked:true,
                split_divider:1,
                newitem:true,
                printbypass:self.printbypass
            }
            }});
            Cart.update(res._id,{$set:{subtotal: sub}});
            Cart.update(res._id,{$set:{gsttotal: gstsub}});
            Cart.update(res._id,{$set:{psttotal: pstsub}});
            Cart.update(res._id,{$set:{total: total}});
        };
        var table = Table.findOne({'name':tableid});
        Table.update(table._id,{$set:{inuse:true}});
        // $('#addtocart_confirm_modal_id').modal('show');
        alert(self.name+'已加入点单！');

    });
}

Template.items.events({

    /**
     *  add new menu item to Cart
     */
    "click .cartbtn":function(event){
        event.preventDefault();
        Session.set('thisItem', this);
        Session.set('specreqitemsValue', " ");
        Session.set('specreqitems'," ");
        var tableid = Session.get("tableid");
        var self=this;

        if (self.SR==1){
        $('#mod2_modal_id').modal('show');            
        $(function(){
           $('input:radio[name=sugar]').filter('[value =全糖]').prop('checked',true);
            $('#fullSugar').addClass('active');
            $('input:radio[name=ice]').filter('[value =正常冰]').prop('checked',true);
            $('#fullIce').addClass('active');
        });   
        }else if(self.SR==2){
        $('#mod3_modal_id').modal('show'); 
        $(function(){
           $('input:radio[name=sugar]').filter('[value =全糖]').prop('checked',true);
            $('#fullSugar').addClass('active');
            $('input:radio[name=ice]').filter('[value =正常冰]').prop('checked',true);
            $('#fullIce').addClass('active');
        });  
        }else if(self.SR==3){ 
        $('#mod4_modal_id').modal('show'); 
        $(function(){
           $('input:radio[name=sugar]').filter('[value =全糖]').prop('checked',true);
            $('#fullSugar').addClass('active');
            $('input:radio[name=ice]').filter('[value =正常冰]').prop('checked',true);
            $('#fullIce').addClass('active');
        });  
        }else if(self.SR==4){ 
        $('#mod5_modal_id').modal('show');                
        }else{
    var cartid = Meteor.call('findCart', tableid, function(err, res){
        if(!res){
            var sub = self.price.toFixed(2);
            var gstsub = (parseFloat(self.price) * parseFloat(self.gst)).toFixed(2);
            var pstsub = (parseFloat(self.price) * parseFloat(self.pst)).toFixed(2);

            var total = (parseFloat(sub) + parseFloat(gstsub) + parseFloat(pstsub)).toFixed(2);

            Cart.insert({
                seqnumber: Meteor.myFunctions.orderInc("userid"),
                tableid:tableid,
                time:new Date(),
                alert:false,
                activated:true,
                paid:false,
                orders:[{
                    seqnumber: Meteor.myFunctions.itemInc("userid"),
                    // itemid:self._id,
                    name:self.name,
                    printbypass:self.printbypass,
                    pic_id:self.pic_id,
                    specreq:" ",
                    price:self.price,
                    gst:gstsub,
                    pst:pstsub,
                    gstrate:self.gst,
                    pstrate:self.pst,
                    checked:true,
                    split_divider:1,
                    newitem:true,
                    printbypass:self.printbypass
                }],
                subtotal:sub,
                gsttotal:gstsub,
                psttotal:pstsub,
                total:total,
                // comments:[{}]
            });
        }else{
            var oldsub = res.subtotal;
            var oldgst = res.gsttotal;
            var oldpst = res.psttotal;
            var oldtotal = res.total;

            var tempsub = self.price.toFixed(2);
            var sub = (parseFloat(oldsub) + parseFloat(tempsub)).toFixed(2);
            var tempgst = (parseFloat(self.price) * parseFloat(self.gst)).toFixed(2);
            var gstsub = (parseFloat(oldgst) + parseFloat(tempgst)).toFixed(2);
            var temppst = (parseFloat(self.price) * parseFloat(self.pst)).toFixed(2);
            var pstsub = (parseFloat(oldpst) + parseFloat(temppst)).toFixed(2);

            var total = (parseFloat(tempsub) + parseFloat(tempgst) + parseFloat(temppst) + parseFloat(oldtotal)).toFixed(2);

            Cart.update(res._id,{ $push: { orders: {
                seqnumber:Meteor.myFunctions.itemInc("userid"),
                // itemid:self._id,
                name:self.name,
                pic_id:self.pic_id,
                specreq:" ",
                price:self.price,
                gst:tempgst,
                pst:temppst,
                gstrate:self.gst,
                pstrate:self.pst,
                checked:true,
                split_divider:1,
                newitem:true,
                printbypass:self.printbypass
            }
            }});
            Cart.update(res._id,{$set:{subtotal: sub}});
            Cart.update(res._id,{$set:{gsttotal: gstsub}});
            Cart.update(res._id,{$set:{psttotal: pstsub}});
            Cart.update(res._id,{$set:{total: total}});
        };
        var table = Table.findOne({'name':tableid});
        Table.update(table._id,{$set:{inuse:true}});
        // $('#addtocart_confirm_modal_id').modal('show');
        alert(self.name+'已加入点单！');

    });       
     };
   }
});



Template.singleitem.helpers({
    item:function(){
        return MenuItem.findOne({seqnumber:Session.get('search_menuitem_seqnumber')});
    }
});

Template.singleitem.events({

    /**
     *  add new menu item to Cart
     */
//    "click .cartbtn":orderMenuItem
});

Template.cart.events({
    "click #cart_confirm_btn":function(event){
        event.preventDefault();
        var tableid = Session.get("tableid");
        var table = Table.findOne({'name':tableid});
        Table.update(table._id,{$set:{inuse:true}});
        Table.update(table._id,{$set:{bing:true}});
        // $('#carttotable_modal_id').modal('show');
        // alert('已通知前台，请稍后');
        var cart = Cart.findOne({'tableid':Session.get("tableid"), "activated":true});
        Session.set('cart',cart);
        Router.go('/posadmin/tablelist/'+tableid);

    },
    "click #goback":function(event){
        event.preventDefault();
        var table = Session.get("tableid");

        var tableroute = '/menu/table/';
        tableroute = tableroute.concat(table);
        //alert(tableroute);
        Router.go(tableroute);
    },
    'click #table_order_btn':function(event){
        event.preventDefault();
        var tableid = Session.get('tableid');
        var cart = Cart.findOne({'tableid':Session.get("tableid"), "activated":true});
        Session.set('cart',cart);
        Router.go('/posadmin/tablelist/'+tableid);
    }
});

Template.menulayout.events({
    'click #confirmOrder':function(e){
        e.preventDefault();
        var tableid = Session.get('tableid');
        var cart = Cart.findOne({'tableid':Session.get("tableid"), "activated":true});
        Session.set('cart',cart);
        Router.go('/posadmin/tablelist/'+tableid);
    }
});

Template.menulayout.helpers({
    cartquantity: function() {
        var tableid = Session.get('tableid');
        var cart = Cart.findOne({'tableid':tableid,"activated":true});
        return cart;
    }
});
