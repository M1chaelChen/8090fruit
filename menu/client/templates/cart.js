Template.cart.events({

    "click .editspecreq":function(event){
        event.preventDefault();
        // var tableselid = event.target.value;
        Session.set('specreq_modal_data',this.seqnumber);
        Session.set('specreqitem',this.specreq);
        $('#itemspecreq').val(Session.get('specreqitem'));
        $('#specreq_modal_id').modal('show');
    },
    "click .mod2btn":function(event){
        event.preventDefault();
        // var moditemname = this.name;
        // var moditemfrommenu = MenuItem.findOne({ name: moditemname });
        // Session.set("cartmoditem", this);
        Session.set('mod2_modal_data', this);
        Session.set('specreqitem', this.specreq);
        $('#mod2item').val(Session.get('specreqitem'));

        $('#mod2_modal_id').modal('show');

    },
    "click .specreqbtn":function(event){
        event.preventDefault();
        var specreq = Session.get('specreqitem');
        var newitem = event.target.value;
        specreq += newitem;
        Session.set('specreqitem', specreq);
        $(event.target).addClass('specreq_selected');
    },
    "click .specreqbtn2":function(event){
        event.preventDefault();
        var specreq = Session.get('specreqitem');
        var newitem = event.target.value;
        specreq += newitem;
        Session.set('specreqitem', specreq);
        $(event.target).addClass('specreq_selected');
    },
    "click .voiditembtn":function(event){
        event.preventDefault();
        var tableid = Session.get('tableid');
        var itemid = Session.get('specreq_modal_data');
        Meteor.call('voidItem',tableid, itemid, "----已取消----", function(err,res){
            //
        });
    }
});





Template.mod2_modal.events({
    "click .mod2select":function(event){
        event.preventDefault();
        $(event.target).addClass('mod2_selected');
        var mod2item = $('#mod2item').val();
        var newmod2item = event.target.name + " ";
        mod2item += newmod2item;
        $('#mod2item').val(mod2item);        
        var mod2itemValue = Session.get('specreqitemsValue');
        var newmod2itemValue = parseFloat(event.target.value);
        var totalValue = parseFloat(mod2itemValue + newmod2itemValue);
        Session.set('specreqitemsValue', totalValue);
        Session.set('specreqitems', mod2item);

    },
    "click #mod2cancel":function(event){
        event.preventDefault();
        $('.mod2select').removeClass('mod2selected');
        $('#mod2item').val(Session.get('specreqitem'));
        Session.set('specreqitems',"");
        Session.set('specreqitemsValue',0);
    },
    "submit #mod2form": function (event) {
        // Prevent default browser form submit
        event.preventDefault();

        // Get value from form element
        var defaultPrice = 0;
        var tableid = Session.get("tableid");
        var specreq = Session.get('specreqitems');
        var specreqSugar =  $("input:radio[name='sugar']:checked").val();
        var specreqIce =  $("input:radio[name='ice']:checked").val();
        var specreqConfirm = specreq + specreqSugar +" " +  specreqIce;
        var specreqValue = (Session.get('specreqitemsValue') + defaultPrice);
        var self = Session.get('thisItem');
        var addHot;
        if($("#hot").is(':checked')){
           addHot = parseFloat(self.hot);
        }
        else{
            addHot = 0;
        }
        var cartid = Meteor.call('findCart', tableid, function(err, res){
        if(!res){
            var sub = (self.price + parseFloat(specreqValue) + parseFloat(addHot)).toFixed(2);
            var gstsub = (parseFloat(sub) * parseFloat(self.gst)).toFixed(2);
            var pstsub = (parseFloat(sub) * parseFloat(self.pst)).toFixed(2);

            var total = (parseFloat(sub) + parseFloat(gstsub) + parseFloat(pstsub)).toFixed(2);

            Cart.insert({
                seqnumber: Meteor.myFunctions.orderInc("userid"),
                tableid:tableid,
                time:new Date(),
                alert:false,
                activated:true,
                paid:false,
                orders:[{
                    seqnumber: Meteor.myFunctions.itemInc("userid"),
                    // itemid:self._id,
                    name:self.name,
                    printbypass:self.printbypass,
                    pic_id:self.pic_id,
                    specreq:specreqConfirm,
                    price:(self.price + parseFloat(specreqValue)+parseFloat(addHot)),
                    gst:gstsub,
                    pst:pstsub,
                    gstrate:self.gst,
                    pstrate:self.pst,
                    checked:true,
                    split_divider:1,
                    newitem:true,
                    printbypass:self.printbypass
                }],
                subtotal:sub,
                gsttotal:gstsub,
                psttotal:pstsub,
                total:total,
                // comments:[{}]
            });
            
        }else{
            var oldsub = res.subtotal;
            var oldgst = res.gsttotal;
            var oldpst = res.psttotal;
            var oldtotal = res.total;

            var tempsub =  (self.price + parseFloat(specreqValue)+parseFloat(addHot)).toFixed(2);
            var sub = (parseFloat(oldsub) + parseFloat(tempsub)).toFixed(2);
            var tempgst = (parseFloat(tempsub) * parseFloat(self.gst)).toFixed(2);
            var gstsub = (parseFloat(oldgst) + parseFloat(tempgst)).toFixed(2);
            var temppst = (parseFloat(tempsub) * parseFloat(self.pst)).toFixed(2);
            var pstsub = (parseFloat(oldpst) + parseFloat(temppst)).toFixed(2);

            var total = (parseFloat(tempsub) + parseFloat(tempgst) + parseFloat(temppst) + parseFloat(oldtotal)).toFixed(2);

            Cart.update(res._id,{ $push: { orders: {
                seqnumber:Meteor.myFunctions.itemInc("userid"),
                // itemid:self._id,
                name:self.name,
                pic_id:self.pic_id,
                specreq:specreqConfirm,
                price:(self.price + parseFloat(specreqValue) + addHot),
                gst:tempgst,
                pst:temppst,
                gstrate:self.gst,
                pstrate:self.pst,
                checked:true,
                split_divider:1,
                newitem:true,
                printbypass:self.printbypass
            }
            }});
            Cart.update(res._id,{$set:{subtotal: sub}});
            Cart.update(res._id,{$set:{gsttotal: gstsub}});
            Cart.update(res._id,{$set:{psttotal: pstsub}});
            Cart.update(res._id,{$set:{total: total}});
        };
        var table = Table.findOne({'name':tableid});
        Table.update(table._id,{$set:{inuse:true}});
        // $('#addtocart_confirm_modal_id').modal('show');
        // alert('已加入点单！');
        });
        
      
        $('#mod2item').val("");
        $('#itemspecreq').val("");
        $(".mod2radiobtn").removeClass('active');
        $('#mod2_modal_id').modal('hide');

    },
});

Template.mod3_modal.events({
    "click .mod3select":function(event){
        event.preventDefault();
        $(event.target).addClass('mod3_selected');
        var mod3item = $('#mod3item').val();
        var newmod3item = event.target.name + " ";
        mod3item += newmod3item;
        $('#mod3item').val(mod3item);        
        var mod3itemValue = Session.get('specreqitemsValue');
        var newmod3itemValue = parseFloat(event.target.value);
        var totalValue = parseFloat(mod3itemValue + newmod3itemValue);
        Session.set('specreqitemsValue', totalValue);
        Session.set('specreqitems', mod3item);

    },
    "click #mod3cancel":function(event){
        event.preventDefault();
        $('.mod3select').removeClass('mod3selected');
        $('#mod3item').val(Session.get('specreqitem'));
        Session.set('specreqitems',"");
        Session.set('specreqitemsValue',0);
    },
    "submit #mod3form": function (event) {
        // Prevent default browser form submit
        event.preventDefault();

        // Get value from form element
        var defaultPrice = 0;
        var tableid = Session.get("tableid");
        var specreq = Session.get('specreqitems');
        var specreqSugar =  $("input:radio[name='sugar']:checked").val();
        var specreqIce =  $("input:radio[name='ice']:checked").val();
        var specreqConfirm = specreq + specreqSugar +" " +  specreqIce;
        var specreqValue = (Session.get('specreqitemsValue') + defaultPrice);
        var self = Session.get('thisItem');
        var addHot;
        if($("#hot").is(':checked')){
           addHot = parseFloat(self.hot);
        }
        else{
            addHot = 0;
        }
        var cartid = Meteor.call('findCart', tableid, function(err, res){
        if(!res){
            var sub = (self.price + parseFloat(specreqValue) + parseFloat(addHot)).toFixed(2);
            var gstsub = (parseFloat(sub) * parseFloat(self.gst)).toFixed(2);
            var pstsub = (parseFloat(sub) * parseFloat(self.pst)).toFixed(2);

            var total = (parseFloat(sub) + parseFloat(gstsub) + parseFloat(pstsub)).toFixed(2);

            Cart.insert({
                seqnumber: Meteor.myFunctions.orderInc("userid"),
                tableid:tableid,
                time:new Date(),
                alert:false,
                activated:true,
                paid:false,
                orders:[{
                    seqnumber: Meteor.myFunctions.itemInc("userid"),
                    // itemid:self._id,
                    name:self.name,
                    printbypass:self.printbypass,
                    pic_id:self.pic_id,
                    specreq:specreqConfirm,
                    price:(self.price + parseFloat(specreqValue)+parseFloat(addHot)),
                    gst:gstsub,
                    pst:pstsub,
                    gstrate:self.gst,
                    pstrate:self.pst,
                    checked:true,
                    split_divider:1,
                    newitem:true,
                    printbypass:self.printbypass
                }],
                subtotal:sub,
                gsttotal:gstsub,
                psttotal:pstsub,
                total:total,
                // comments:[{}]
            });
            
        }else{
            var oldsub = res.subtotal;
            var oldgst = res.gsttotal;
            var oldpst = res.psttotal;
            var oldtotal = res.total;

            var tempsub =  (self.price + parseFloat(specreqValue)+parseFloat(addHot)).toFixed(2);
            var sub = (parseFloat(oldsub) + parseFloat(tempsub)).toFixed(2);
            var tempgst = (parseFloat(tempsub) * parseFloat(self.gst)).toFixed(2);
            var gstsub = (parseFloat(oldgst) + parseFloat(tempgst)).toFixed(2);
            var temppst = (parseFloat(tempsub) * parseFloat(self.pst)).toFixed(2);
            var pstsub = (parseFloat(oldpst) + parseFloat(temppst)).toFixed(2);

            var total = (parseFloat(tempsub) + parseFloat(tempgst) + parseFloat(temppst) + parseFloat(oldtotal)).toFixed(2);

            Cart.update(res._id,{ $push: { orders: {
                seqnumber:Meteor.myFunctions.itemInc("userid"),
                // itemid:self._id,
                name:self.name,
                pic_id:self.pic_id,
                specreq:specreqConfirm,
                price:(self.price + parseFloat(specreqValue) + addHot),
                gst:tempgst,
                pst:temppst,
                gstrate:self.gst,
                pstrate:self.pst,
                checked:true,
                split_divider:1,
                newitem:true,
                printbypass:self.printbypass
            }
            }});
            Cart.update(res._id,{$set:{subtotal: sub}});
            Cart.update(res._id,{$set:{gsttotal: gstsub}});
            Cart.update(res._id,{$set:{psttotal: pstsub}});
            Cart.update(res._id,{$set:{total: total}});
        };
        var table = Table.findOne({'name':tableid});
        Table.update(table._id,{$set:{inuse:true}});
        // $('#addtocart_confirm_modal_id').modal('show');
        // alert('已加入点单！');
        });
        
        $('#mod3item').val("");
        $('#itemspecreq').val("");
        $(".mod3radiobtn").removeClass('active');
        $('#mod3_modal_id').modal('hide');

    },
});

Template.mod4_modal.events({
    "click .mod4select":function(event){
        event.preventDefault();
        $(event.target).addClass('mod4_selected');
        var mod4item = $('#mod4item').val();
        var newmod4item = event.target.name + " ";
        mod4item += newmod4item;
        $('#mod4item').val(mod4item);        
        var mod4itemValue = Session.get('specreqitemsValue');
        var newmod4itemValue = parseFloat(event.target.value);
        var totalValue = parseFloat(mod4itemValue + newmod4itemValue);
        Session.set('specreqitemsValue', totalValue);
        Session.set('specreqitems', mod4item);

    },
    "click #mod4cancel":function(event){
        event.preventDefault();
        $('.mod4select').removeClass('mod4selected');
        $('#mod4item').val(Session.get('specreqitem'));
        Session.set('specreqitems',"");
        Session.set('specreqitemsValue',0);
    },
    "submit #mod4form": function (event) {
        // Prevent default browser form submit
        event.preventDefault();

        // Get value from form element
        var defaultPrice = 0;
        var tableid = Session.get("tableid");
        var specreq = Session.get('specreqitems');
        var specreqSugar =  $("input:radio[name='sugar']:checked").val();
        var specreqIce =  $("input:radio[name='ice']:checked").val();
        var specreqConfirm = specreq + specreqSugar +" " +  specreqIce;
        var specreqValue = (Session.get('specreqitemsValue') + defaultPrice);
        var self = Session.get('thisItem');
        var addHot;
        if($("#hot").is(':checked')){
           addHot = parseFloat(self.hot);
        }
        else{
            addHot = 0;
        }
        var cartid = Meteor.call('findCart', tableid, function(err, res){
        if(!res){
            var sub = (self.price + parseFloat(specreqValue) + parseFloat(addHot)).toFixed(2);
            var gstsub = (parseFloat(sub) * parseFloat(self.gst)).toFixed(2);
            var pstsub = (parseFloat(sub) * parseFloat(self.pst)).toFixed(2);

            var total = (parseFloat(sub) + parseFloat(gstsub) + parseFloat(pstsub)).toFixed(2);

            Cart.insert({
                seqnumber: Meteor.myFunctions.orderInc("userid"),
                tableid:tableid,
                time:new Date(),
                alert:false,
                activated:true,
                paid:false,
                orders:[{
                    seqnumber: Meteor.myFunctions.itemInc("userid"),
                    // itemid:self._id,
                    name:self.name,
                    printbypass:self.printbypass,
                    pic_id:self.pic_id,
                    specreq:specreqConfirm,
                    price:(self.price + parseFloat(specreqValue)+parseFloat(addHot)),
                    gst:gstsub,
                    pst:pstsub,
                    gstrate:self.gst,
                    pstrate:self.pst,
                    checked:true,
                    split_divider:1,
                    newitem:true,
                    printbypass:self.printbypass
                }],
                subtotal:sub,
                gsttotal:gstsub,
                psttotal:pstsub,
                total:total,
                // comments:[{}]
            });
            
        }else{
            var oldsub = res.subtotal;
            var oldgst = res.gsttotal;
            var oldpst = res.psttotal;
            var oldtotal = res.total;

            var tempsub =  (self.price + parseFloat(specreqValue)+parseFloat(addHot)).toFixed(2);
            var sub = (parseFloat(oldsub) + parseFloat(tempsub)).toFixed(2);
            var tempgst = (parseFloat(tempsub) * parseFloat(self.gst)).toFixed(2);
            var gstsub = (parseFloat(oldgst) + parseFloat(tempgst)).toFixed(2);
            var temppst = (parseFloat(tempsub) * parseFloat(self.pst)).toFixed(2);
            var pstsub = (parseFloat(oldpst) + parseFloat(temppst)).toFixed(2);

            var total = (parseFloat(tempsub) + parseFloat(tempgst) + parseFloat(temppst) + parseFloat(oldtotal)).toFixed(2);

            Cart.update(res._id,{ $push: { orders: {
                seqnumber:Meteor.myFunctions.itemInc("userid"),
                // itemid:self._id,
                name:self.name,
                pic_id:self.pic_id,
                specreq:specreqConfirm,
                price:(self.price + parseFloat(specreqValue) + addHot),
                gst:tempgst,
                pst:temppst,
                gstrate:self.gst,
                pstrate:self.pst,
                checked:true,
                split_divider:1,
                newitem:true,
                printbypass:self.printbypass
            }
            }});
            Cart.update(res._id,{$set:{subtotal: sub}});
            Cart.update(res._id,{$set:{gsttotal: gstsub}});
            Cart.update(res._id,{$set:{psttotal: pstsub}});
            Cart.update(res._id,{$set:{total: total}});
        };
        var table = Table.findOne({'name':tableid});
        Table.update(table._id,{$set:{inuse:true}});
        // $('#addtocart_confirm_modal_id').modal('show');
        // alert('已加入点单！');
        });
        
        $('#mod4item').val("");
        $('#itemspecreq').val("");
        $(".mod4radiobtn").removeClass('active');
        $('#mod4_modal_id').modal('hide');

    },
});


Template.mod5_modal.events({
    "click .mod5select":function(event){
        event.preventDefault();
        $(event.target).addClass('mod5_selected');
        var mod5item = $('#mod5item').val();
        var newmod5item = event.target.name + " ";
        mod5item += newmod5item;
        $('#mod5item').val(mod5item);        
        var mod5itemValue = Session.get('specreqitemsValue');
        var newmod5itemValue = parseFloat(event.target.value);
        var totalValue = parseFloat(mod5itemValue + newmod5itemValue);
        Session.set('specreqitemsValue', totalValue);
        Session.set('specreqitems', mod5item);
        


    },
    "click #mod5cancel":function(event){
        event.preventDefault();
        $('.mod5select').removeClass('mod5selected');
        $('#mod5item').val(Session.get('specreqitem'));
        Session.set('specreqitems',"");
        Session.set('specreqitemsValue',0);
    },
    "submit #mod5form": function (event) {
        // Prevent default browser form submit
        event.preventDefault();

        // Get value from form element
        var defaultPrice = 0;
        var tableid = Session.get("tableid");
        var specreq = Session.get('specreqitems');
//        var specreqSugar =  $("input:radio[name='sugar']:checked").val();
//        var specreqIce =  $("input:radio[name='ice']:checked").val();
        var specreqConfirm = specreq ;
        var specreqValue = (Session.get('specreqitemsValue') + defaultPrice);
        var self = Session.get('thisItem');

   
        var cartid = Meteor.call('findCart', tableid, function(err, res){
        if(!res){
            var sub = (self.price + parseFloat(specreqValue)).toFixed(2);
            var gstsub = (parseFloat(sub) * parseFloat(self.gst)).toFixed(2);
            var pstsub = (parseFloat(sub) * parseFloat(self.pst)).toFixed(2);

            var total = (parseFloat(sub) + parseFloat(gstsub) + parseFloat(pstsub)).toFixed(2);

            Cart.insert({
                seqnumber: Meteor.myFunctions.orderInc("userid"),
                tableid:tableid,
                time:new Date(),
                alert:false,
                activated:true,
                paid:false,
                orders:[{
                    seqnumber: Meteor.myFunctions.itemInc("userid"),
                    // itemid:self._id,
                    name:self.name,
                    printbypass:self.printbypass,
                    pic_id:self.pic_id,
                    specreq:specreqConfirm,
                    price:(self.price + parseFloat(specreqValue)),
                    gst:gstsub,
                    pst:pstsub,
                    gstrate:self.gst,
                    pstrate:self.pst,
                    checked:true,
                    split_divider:1,
                    newitem:true,
                    printbypass:self.printbypass
                }],
                subtotal:sub,
                gsttotal:gstsub,
                psttotal:pstsub,
                total:total,
                // comments:[{}]
            });
            
        }else{
            var oldsub = res.subtotal;
            var oldgst = res.gsttotal;
            var oldpst = res.psttotal;
            var oldtotal = res.total;

            var tempsub =  (self.price + parseFloat(specreqValue)+parseFloat(addHot)).toFixed(2);
            var sub = (parseFloat(oldsub) + parseFloat(tempsub)).toFixed(2);
            var tempgst = (parseFloat(tempsub) * parseFloat(self.gst)).toFixed(2);
            var gstsub = (parseFloat(oldgst) + parseFloat(tempgst)).toFixed(2);
            var temppst = (parseFloat(tempsub) * parseFloat(self.pst)).toFixed(2);
            var pstsub = (parseFloat(oldpst) + parseFloat(temppst)).toFixed(2);

            var total = (parseFloat(tempsub) + parseFloat(tempgst) + parseFloat(temppst) + parseFloat(oldtotal)).toFixed(2);

            Cart.update(res._id,{ $push: { orders: {
                seqnumber:Meteor.myFunctions.itemInc("userid"),
                // itemid:self._id,
                name:self.name,
                pic_id:self.pic_id,
                specreq:specreqConfirm,
                price:(self.price + parseFloat(specreqValue) + addHot),
                gst:tempgst,
                pst:temppst,
                gstrate:self.gst,
                pstrate:self.pst,
                checked:true,
                split_divider:1,
                newitem:true,
                printbypass:self.printbypass
            }
            }});
            Cart.update(res._id,{$set:{subtotal: sub}});
            Cart.update(res._id,{$set:{gsttotal: gstsub}});
            Cart.update(res._id,{$set:{psttotal: pstsub}});
            Cart.update(res._id,{$set:{total: total}});
        };
        var table = Table.findOne({'name':tableid});
        Table.update(table._id,{$set:{inuse:true}});
        // $('#addtocart_confirm_modal_id').modal('show');
        // alert('已加入点单！');
        });
        

        $('#itemspecreq').val("");
        $(".mod5radiobtn").removeClass('active');
        $('#mod5_modal_id').modal('hide');
        $('#mod5item').val(Session.get('specreqitem'));
        
    },
});