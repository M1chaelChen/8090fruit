Meteor.methods({
	findCart: function(name) {
		var ret = Cart.findOne({"tableid": name, "activated":true});
		return ret;
	},
	updateSpecreq: function(tableid, itemseq, spec){
		Cart.update({"tableid":tableid, "activated":true, "orders.seqnumber":itemseq } ,{ $set: { "orders.$.specreq" : spec}});
		return "已加入！";
	},
	voidItem: function(tableid, itemseq, spec){
		Cart.update({"tableid":tableid, "activated":true, "orders.seqnumber":itemseq } ,{ $set: { "orders.$.specreq" : spec, "voided":true}});
		return "已加入！";
	}
});