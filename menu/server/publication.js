Meteor.publish("category", function () {
    return Category.find();
});

Meteor.publish("menuitem", function () {
    return MenuItem.find();
});

Meteor.publish("cart", function () {
    return Cart.find();
});

Meteor.publish("order_counter", function () {
    return OrderSeq.find();
});

Meteor.publish("item_counter", function () {
    return ItemSeq.find();
});

Meteor.publish("receipt_counter", function () {
    return ReceiptSeq.find();
});