Router.route('/', {
    action: function () {
        if (!Meteor.user()) {
            this.render('login');
        }else{
            Router.go('/home');
        }
    }
});

var requireLogin = function(){
    if (!Meteor.user()){
      this.render('login');
    }
    else {
      this.next();
    }
}

Router.onBeforeAction(requireLogin);
