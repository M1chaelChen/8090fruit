Meteor.startup(function(){
	console.log('Server started');
  
	// #Users and Permissions -> -> Creating the admin user
	if(Meteor.users.find().count() === 0) {

		console.log('Created Admin user');

		var userId = Accounts.createUser({
			username: '8090fruit',
			password: '8090',
			profile: {
				name: 'Admin'
			}
		});
		Meteor.users.update(userId, {$set: {
			roles: {admin: true},
		}})
	}
});
