
Router.configure({
    // the default layout
    layoutTemplate: 'mainlayout'
});

Router.route('/home', function () {
    this.render('home');
    this.layout('mainlayout');
});
