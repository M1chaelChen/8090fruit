Template.registerHelper('equals', function (a, b) {
    return a === b;
});

Template.kitchenview.helpers({
    carts:function(){
        var carts= Cart.find();
        return carts;
    }
});


Template.kitchenview.events({

    "click .ordermade":function(event){
        //alert(Session.get("tableid"));
        var itemid=$(event.target).data('index');

        var carts= Cart.find();

        carts.forEach(function(cart){
            for(var i=0;i<cart.orders.length;i++){
                if(cart.orders[i].itemid==itemid){
                    var str={};
                    str["orders."+i+".status"]='done';
                    Cart.update(cart._id,{ $set: str});
                    var table=Table.findOne({number:parseInt(cart.tableid)});
                    Table.update(table._id,{$set:{bing:true}});
                }
            }
        });
    }
});

